/*
 * Copyright 2015 Matthew Aguirre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tros.logo.android;

import org.tros.logo.LogoCanvas;
import org.tros.torgo.android.ILogoDrawable;
import org.tros.torgo.android.ILogoOutputDisplay;

/**
 * Created by matta on 6/5/15.
 */
public class AndroidCanvas implements LogoCanvas {

    //HACK: get the instance created during init:
    public static ILogoDrawable customCanvas;
    public static ILogoOutputDisplay outputWriter;
    public static boolean IMPLIED_PEN_UP_ON_BACKWARD = false;
    private boolean penup;
    private boolean showTurtle;
    private double angle;
    private double penX;
    private double penY;

    public AndroidCanvas() {
        penup = false;
        showTurtle = true;
     }

    @Override
    public void backward(double distance) {
        double newx = penX - (distance * Math.cos(angle));
        double newy = penY - (distance * Math.sin(angle));

        if (!penup && customCanvas != null && !IMPLIED_PEN_UP_ON_BACKWARD) {
            customCanvas.drawLine((float) penX, (float) penY, (float) newx, (float) newy);
        }

        penX = newx;
        penY = newy;
    }

    @Override
    public void canvascolor(int red, int green, int blue) {
        red = Math.min(255, Math.max(0, red));
        green = Math.min(255, Math.max(0, green));
        blue = Math.min(255, Math.max(0, blue));
        customCanvas.setCanvasColor(red, green, blue);
    }

    @Override
    public void canvascolor(String color) {
        if (customCanvas != null) {
            customCanvas.setCanvasColor(color);
        }
    }

    @Override
    public void clear() {
        if (customCanvas != null) {
            customCanvas.clearCanvas();
        }
    }

    @Override
    public void drawString(String message) {
        if (!penup && customCanvas != null) {
            customCanvas.drawString(message, (float) penX, (float) penY, (float) angle);
//            AffineTransform saveXform = g2.getTransform();
//            //double offsetAngle = (Math.PI / 2.0);
//            double offsetAngle = 0;
//            g2.setTransform(AffineTransform.getRotateInstance(angle + offsetAngle, penX, penY));
//            g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
//            g2.drawString(message, (int) penX, (int) penY);
//            g2.setTransform(saveXform);
        }
    }

    @Override
    public void fontName(String fontFace) {
//        int style = font.getStyle();
//        int size = font.getSize();
//
//        font = new Font(fontFace, style, size);
//
//        g2.setFont(font);
    }

    @Override
    public void fontSize(int size) {
//        String fontName = font.getFontName();
//        int style = font.getStyle();
//
//        font = new Font(fontName, style, size);
//
//        g2.setFont(font);
    }

    @Override
    public void fontStyle(int style) {
//        String fontName = font.getFontName();
//        int size = font.getSize();
//
//        font = new Font(fontName, style, size);
//
//        g2.setFont(font);
    }

    @Override
    public void forward(double distance) {
        double newx = penX + (distance * Math.cos(angle));
        double newy = penY + (distance * Math.sin(angle));

        if (!penup && customCanvas != null) {
            customCanvas.drawLine((float) penX, (float) penY, (float) newx, (float) newy);
        }

        penX = newx;
        penY = newy;
    }

    @Override
    public void hideTurtle() {
        showTurtle = false;
        if (customCanvas != null) {
            customCanvas.showTurtle(showTurtle);
        }
    }

    @Override
    public void home() {
//        if (customCanvas != null && CanvasView.class.isAssignableFrom(customCanvas.getClass())) {
//            penX = customCanvas.getWidth() / 2.0;
//            penY = customCanvas.getHeight() / 2.0;
//            angle = -1.0 * (Math.PI / 2.0);
//        } else {
            penX = 0;
            penY = 0;
            angle = 1.0 * (Math.PI / 2.0);
//        }
    }

    @Override
    public void left(double angle) {
        this.angle -= Math.PI * angle / 180.0;
    }

    @Override
    public void pause(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void penDown() {
        this.penup = false;
    }

    @Override
    public void penUp() {
        this.penup = true;
    }

    @Override
    public void pencolor(int red, int green, int blue, int alpha) {
        red = Math.min(255, Math.max(0, red));
        green = Math.min(255, Math.max(0, green));
        blue = Math.min(255, Math.max(0, blue));
        if (customCanvas != null) {
            customCanvas.setPaintColor(red, green, blue, alpha);
        }
    }

    @Override
    public void pencolor(String color) {
        if (customCanvas != null) {
            customCanvas.setPaintColor(color);
        }
    }

    @Override
    public void repaint() {
        if (customCanvas != null) {
            customCanvas.turtleUpdate((float) angle, (float) penX, (float) penY);
            customCanvas.showTurtle(showTurtle);
        }
//        if (customCanvas != null && CanvasView.class.isAssignableFrom(customCanvas.getClass())) {
//            customCanvas.postInvalidate();
//        }
    }

    @Override
    public void message(String message) {
        if (outputWriter != null) {
            outputWriter.message(message);
        }
    }

    @Override
    public void warning(String message) {
        if (outputWriter != null) {
            outputWriter.warning(message);
        }
    }

    @Override
    public void right(double angle) {
        this.angle += Math.PI * angle / 180.0;
    }

    @Override
    public void setXY(double x, double y) {
        if (customCanvas != null) {
            x = (customCanvas.getWidth()) / 2.0 + x;
            y = (customCanvas.getHeight()) / 2.0 + y;

            if (!penup) {
                customCanvas.drawLine((float) penX, (float) penY, (float) x, (float) y);
            }
            penX = x;
            penY = y;
        }
    }

    @Override
    public void showTurtle() {
        showTurtle = true;
        if (customCanvas != null) {
            customCanvas.showTurtle(showTurtle);
        }
    }

    @Override
    public double getTurtleX() {
        return penX;
    }

    @Override
    public double getTurtleY() {
        return penY;
    }

    @Override
    public double getTurtleAngle() {
        return angle;
    }
}
