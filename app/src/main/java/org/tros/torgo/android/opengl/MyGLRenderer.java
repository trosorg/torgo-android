/*
 * Copyright 2015 Matthew Aguirre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tros.torgo.android.opengl;

import android.opengl.GLES20;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;

import org.tros.torgo.R;
import org.tros.torgo.android.TorgoApp;

/**
 * Created by matta on 6/21/15.
 */
public class MyGLRenderer implements GLSurfaceView.Renderer {
    private float[] bgColor = new float[]{1.0f, 1.0f, 1.0f, 1.0f};
    private Lines lines;
    protected Sprite turtle;

    protected float scale = 1.0f;
    protected float xTranslate = 0.0f;
    protected float yTranslate = 0.0f;

    private final float[] mMVPMatrix = new float[16];
    private final float[] mProjectionMatrix = new float[16];
    private final float[] mViewMatrix = new float[16];

    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        // Set the background frame color
        GLES20.glClearColor(bgColor[0], bgColor[0], bgColor[0], bgColor[0]);
        turtle = new Sprite(TorgoApp.getContext(), R.drawable.turtle);
        lines = new Lines();
    }

    public void addLine(Lines.Line line) {
        lines.addLine(line);
    }

    public void clear() {
        lines.clear();
    }

    private boolean showTurtle;

    public void showTurtle(boolean value) {
        this.showTurtle = value;
    }

    public void turtlePos(float angle, float x, float y) {
        if (turtle != null) {
            turtle.setAngle(angle);
            turtle.setX(x);
            turtle.setY(y);
        }
    }

    public void setBgColor(int r, int b, int g, float a) {
        bgColor[0] = r / 255.0f;
        bgColor[1] = g / 255.0f;
        bgColor[2] = b / 255.0f;
        bgColor[3] = a / 255.0f;
        GLES20.glClearColor(bgColor[0], bgColor[0], bgColor[0], bgColor[0]);
    }

    public void onDrawFrame(GL10 unused) {
        // Draw background color
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        // Set the camera position (View matrix)
        Matrix.setLookAtM(mViewMatrix, 0, 0, 0, -3, 0f, 0f, 0f, 0f, 1.0f, 0.0f);

        lines.draw(mMVPMatrix);
        if (showTurtle && turtle != null) {
            // Calculate the projection and view transformation
            Matrix.multiplyMM(mMVPMatrix, 0, mProjectionMatrix, 0, mViewMatrix, 0);
            Matrix.translateM(mMVPMatrix, 0, xTranslate, yTranslate, 0);
            Matrix.scaleM(mMVPMatrix, 0, scale, scale, 0);

            turtle.draw(mMVPMatrix);
        }
    }

    @Override
    public void onSurfaceChanged(GL10 unused, int width, int height) {
        // Adjust the viewport based on geometry changes,
        // such as screen rotation
        GLES20.glViewport(0, 0, width, height);

        float ratio = (float) width / height;

        // this projection matrix is applied to object coordinates
        // in the onDrawFrame() method
        Matrix.frustumM(mProjectionMatrix, 0, -ratio, ratio, -1, 1, 3, 7);
    }

    /**
     * Utility method for compiling a OpenGL shader.
     *
     * <p><strong>Note:</strong> When developing shaders, use the checkGlError()
     * method to debug shader coding errors.</p>
     *
     * @param type - Vertex or fragment shader type.
     * @param shaderCode - String containing the shader code.
     * @return - Returns an id for the shader.
     */
    public static int loadShader(int type, String shaderCode){

        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);

        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);

        return shader;
    }
}
