///*
// * Copyright 2015 Matthew Aguirre
// *
// * Licensed under the Apache License, Version 2.0 (the "License");
// * you may not use this file except in compliance with the License.
// * You may obtain a copy of the License at
// *
// *     http://www.apache.org/licenses/LICENSE-2.0
// *
// * Unless required by applicable law or agreed to in writing, software
// * distributed under the License is distributed on an "AS IS" BASIS,
// * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// * See the License for the specific language governing permissions and
// * limitations under the License.
// */
//package org.tros.torgo.android;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.graphics.Canvas;
//import android.graphics.Color;
//import android.graphics.Paint;
//import android.util.AttributeSet;
//import android.view.View;
//import android.widget.ImageView;
//
//public class CanvasView extends ImageView implements ILogoDrawable, ISizeChangeListener {
//
//    private Bitmap mBitmap;
//    private Canvas mCanvas;
//    private Paint mPaint;
//
//    private final org.apache.commons.lang3.event.EventListenerSupport<Runnable> onSizeChange = org.apache.commons.lang3.event.EventListenerSupport.create(Runnable.class);
//
//    public CanvasView(Context c, AttributeSet attrs) {
//        super(c, attrs);
//
//        // and we set a new Paint with the desired attributes
//        mPaint = new Paint();
//        mPaint.setAntiAlias(true);
//        mPaint.setColor(Color.BLACK);
//        mPaint.setStyle(Paint.Style.STROKE);
//        mPaint.setStrokeJoin(Paint.Join.ROUND);
//        mPaint.setStrokeWidth(1f);
//    }
//
//    public CanvasView(Context c) {
//        super(c);
//
//        // and we set a new Paint with the desired attributes
//        mPaint = new Paint();
//        mPaint.setAntiAlias(true);
//        mPaint.setColor(Color.BLACK);
//        mPaint.setStyle(Paint.Style.STROKE);
//        mPaint.setStrokeJoin(Paint.Join.ROUND);
//        mPaint.setStrokeWidth(1f);
//    }
//
//    // override onSizeChanged
//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//
//        // your Canvas will draw onto the defined Bitmap
//        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
//        mCanvas = new Canvas(mBitmap);
//        this.setImageBitmap(mBitmap);
//        onSizeChange.fire().run();
//    }
//
//    public void addOnSizeChangeListener(Runnable runner) {
//        onSizeChange.addListener(runner);
//    }
//
//    public boolean isReady() {
//        return mCanvas != null;
//    }
//
//    @Override
//    public void setPaintColor(String color) {
//        mPaint = new Paint(mPaint);
//        mPaint.setColor(Color.parseColor(color));
//    }
//
//    @Override
//    public void setCanvasColor(String color) {
////        mPaint = new Paint(mPaint);
////        mPaint.setColor(Color.parseColor(color));
//    }
//
//    @Override
//    public void setPaintColor(int r, int g, int b, int a) {
//        mPaint = new Paint(mPaint);
//        mPaint.setColor(Color.argb(a, r, g, b));
//    }
//
//    @Override
//    public void setCanvasColor(int r, int g, int b) {
////        mPaint = new Paint(mPaint);
////        mPaint.setColor(Color.argb(a, r, g, b));
//    }
//
//    @Override
//    public void drawString(String text, float x, float y, float angle) {
//        if (mCanvas != null) {
//            mCanvas.drawText(text, x, y, mPaint);
//        }
//    }
//
//    // override onDraw
//    @Override
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//        if (mBitmap != null) {
//            setImageBitmap(mBitmap);
//        }
////        if (mBitmap != null) {
////            canvas.drawBitmap(mBitmap, 0, 0, null);
////        }
//    }
//
//    @Override
//    public void drawLine(float x1, float y1, float x2, float y2) {
//        if (mBitmap != null) {
//            mCanvas.drawLine(x1, y1, x2, y2, mPaint);
//        }
//    }
//
//    @Override
//    public void clearCanvas() {
//        if (mBitmap != null) {
//            mBitmap = Bitmap.createBitmap(mBitmap.getWidth(), mBitmap.getHeight(), Bitmap.Config.ARGB_8888);
//            mCanvas = new Canvas(mBitmap);
//        }
//    }
//
//    @Override
//    public void showTurtle(boolean value) { }
//}