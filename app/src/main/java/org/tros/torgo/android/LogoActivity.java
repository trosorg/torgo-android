/*
 * Copyright 2015 Matthew Aguirre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tros.torgo.android;

import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.FloatMath;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.tros.logo.android.AndroidCanvas;
import org.tros.torgo.Controller;
import org.tros.torgo.R;
import org.tros.torgo.TorgoToolkit;
import org.tros.torgo.android.opengl.MyGLSurfaceView;
import org.tros.torgo.interpreter.CodeBlock;
import org.tros.torgo.interpreter.InterpreterListener;
import org.tros.torgo.interpreter.Scope;


public class LogoActivity extends ActionBarActivity implements ActionBar.TabListener {

    private static final org.tros.utils.logging.Logger logger = org.tros.utils.logging.Logging.getLogFactory().getLogger(LogoActivity.class);
    private final TextFragment textConsole = TextFragment.newInstance(2);
    private final GraphicFragment graphicConsole = GraphicFragment.newInstance(1);

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo);

        final AtomicInteger counter = new AtomicInteger(0);
        LogoFrag.counter = counter;

        Intent intent = getIntent();
        final String source = intent.getStringExtra(SourceEditActivity.SOURCE_MESSAGE);
        final Controller c = TorgoToolkit.getController("logo");
        c.run();
        c.setSource(source);
        c.addInterpreterListener(new InterpreterListener() {
            @Override
            public void started() {

            }

            @Override
            public void finished() {
                AndroidCanvas.customCanvas = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        logger.verbose("Complete!");
                        textConsole.appendText("Complete!");
                    }
                });
            }

            @Override
            public void error(final Exception e) {
                AndroidCanvas.customCanvas = null;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        logger.warn("Error: " + e.getMessage());
                        textConsole.appendText(e.getMessage());
                    }
                });
            }

            @Override
            public void message(String msg) {
                textConsole.appendText(msg);
            }

            @Override
            public void currStatement(CodeBlock block, Scope scope) {

            }
        });
        LogoFrag.controller = c;

        // Set up the action bar.
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        LogoFrag.settings = settings;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                actionBar.setSelectedNavigationItem(1);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        graphicConsole.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        graphicConsole.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_logo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
//        if (tab.getPosition() == 0) {
//            graphicConsole.onResume();
//        } else {
//            graphicConsole.onPause();
//        }
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Locale l = Locale.getDefault();
            switch (position) {
                case 1:
                    return graphicConsole;// GraphicFragment.newInstance(position + 1);
                case 0:
                    return textConsole; // TextFragment.newInstance(position + 1);
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 1:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 0:
                    return getString(R.string.title_section2).toUpperCase(l);
            }
            return null;
        }
    }

    public static abstract class LogoFrag extends Fragment {
        protected static final String ARG_SECTION_NUMBER = "section_number";
        protected TextView console;
        protected View rootView;
        static protected AtomicInteger counter;
        static protected Controller controller;
        static protected SharedPreferences settings;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class TextFragment extends LogoFrag implements ILogoOutputDisplay {
        /**
         * Retu
         * rns a new instance of this fragment for the given section
         * number.
         */
        public static TextFragment newInstance(int sectionNumber) {
            TextFragment fragment = new TextFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public TextFragment() {
            AndroidCanvas.outputWriter = this;
        }

        public void appendText(final String text) {
            FragmentActivity activity = getActivity();
            if (console != null && activity != null) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        console.append(text + "\n");
                        rootView.postInvalidate();
                    }
                });
            }
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            try {
                rootView = inflater.inflate(R.layout.fragment_logo_text_console, container, false);
                console = (TextView) rootView.findViewById(R.id.text_view_console);

                String font_size = settings.getString("font_size", "");
                if (!"".equals(font_size)) {
                    try {
                        int f = Integer.parseInt(font_size);
                        console.setTextSize(TypedValue.COMPLEX_UNIT_DIP, f);
                    } catch (NumberFormatException nfe) {
                    }
                }

                counter.incrementAndGet();
            }
            catch(Exception ex) {
                System.err.println(ex.getMessage());
            }
            return rootView;
        }

        @Override
        public void message(String message) {
            appendText(message);
        }

        @Override
        public void warning(String warning) {
            appendText(warning);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            AndroidCanvas.outputWriter = null;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            AndroidCanvas.outputWriter = null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class GraphicFragment extends LogoFrag implements View.OnTouchListener {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */

        private static final String TAG = "Touch";
        @SuppressWarnings("unused")
        private static final float MIN_ZOOM = 1f,MAX_ZOOM = 1f;

        // These matrices will be used to scale points of the image
        Matrix matrix = new Matrix();
        Matrix savedMatrix = new Matrix();

        // The 3 states (events) which the user is trying to perform
        static final int NONE = 0;
        static final int DRAG = 1;
        static final int ZOOM = 2;
        int mode = NONE;

        // these PointF objects are used to record the point(s) the user is touching
        PointF start = new PointF();
        PointF mid = new PointF();
        float oldDist = 1f;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static GraphicFragment newInstance(int sectionNumber) {
            GraphicFragment fragment = new GraphicFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public GraphicFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            if (container == null) {
                return null;
            }

            rootView = inflater.inflate(R.layout.fragment_logo_graphics_console, container, false);

            controller.stopInterpreter();

            final View view = (View) rootView.findViewById(R.id.logo_canvas);
            if (ILogoDrawable.class.isAssignableFrom(view.getClass())) {
                AndroidCanvas.customCanvas = (ILogoDrawable) view;
            }

//            if (MyGLSurfaceView.class.isAssignableFrom(view.getClass())) {
//            } else if (org.tros.torgo.android.CanvasView.class.isAssignableFrom(view.getClass())) {
//                view.setOnTouchListener(this);
//            }

            if (ISizeChangeListener.class.isAssignableFrom(view.getClass())) {
                ((ISizeChangeListener)view).addOnSizeChangeListener(new Runnable() {
                @Override
                public void run() {
                        Thread startThread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                while (counter.get() < 2) {
                                }
                                //HACK: seems to make it so that the beginning of a script is not cleared.
                                try {
                                    Thread.sleep(250);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                controller.startInterpreter();
                            }
                        });
                        startThread.start();
                    }
                });

                counter.incrementAndGet();
            } else {
                controller.startInterpreter();
            }

            view.setKeepScreenOn(true);

            return rootView;
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            controller.stopInterpreter();
            AndroidCanvas.customCanvas = null;
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            controller.stopInterpreter();
            AndroidCanvas.customCanvas = null;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            ImageView view = (ImageView) v;
            view.setScaleType(ImageView.ScaleType.MATRIX);
            float scale;

            switch (event.getAction() & MotionEvent.ACTION_MASK)
            {
                case MotionEvent.ACTION_DOWN:   // first finger down only
                    savedMatrix.set(matrix);
                    start.set(event.getX(), event.getY());
                    android.util.Log.d(TAG, "mode=DRAG"); // write to LogCat
                    mode = DRAG;
                    break;

                case MotionEvent.ACTION_UP: // first finger lifted

                case MotionEvent.ACTION_POINTER_UP: // second finger lifted

                    mode = NONE;
                    android.util.Log.d(TAG, "mode=NONE");
                    break;

                case MotionEvent.ACTION_POINTER_DOWN: // first and second finger down

                    oldDist = spacing(event);
                    android.util.Log.d(TAG, "oldDist=" + oldDist);
                    if (oldDist > 5f) {
                        savedMatrix.set(matrix);
                        midPoint(mid, event);
                        mode = ZOOM;
                        android.util.Log.d(TAG, "mode=ZOOM");
                    }
                    break;

                case MotionEvent.ACTION_MOVE:

                    if (mode == DRAG)
                    {
                        matrix.set(savedMatrix);
                        matrix.postTranslate(event.getX() - start.x, event.getY() - start.y); // create the transformation in the matrix  of points
                    }
                    else if (mode == ZOOM)
                    {
                        // pinch zooming
                        float newDist = spacing(event);
                        android.util.Log.d(TAG, "newDist=" + newDist);
                        if (newDist > 5f)
                        {
                            matrix.set(savedMatrix);
                            scale = newDist / oldDist; // setting the scaling of the
                            // matrix...if scale > 1 means
                            // zoom in...if scale < 1 means
                            // zoom out
                            matrix.postScale(scale, scale, mid.x, mid.y);
                        }
                    }
                    break;
            }

            view.setImageMatrix(matrix); // display the transformation on screen

            return true; // indicate event was handled
        }

        /*
         * --------------------------------------------------------------------------
         * Method: spacing Parameters: MotionEvent Returns: float Description:
         * checks the spacing between the two fingers on touch
         * ----------------------------------------------------
         */
        private float spacing(MotionEvent event)
        {
            float x = event.getX(0) - event.getX(1);
            float y = event.getY(0) - event.getY(1);
            return FloatMath.sqrt(x * x + y * y);
        }

        /*
         * --------------------------------------------------------------------------
         * Method: midPoint Parameters: PointF object, MotionEvent Returns: void
         * Description: calculates the midpoint between the two fingers
         * ------------------------------------------------------------
         */
        private void midPoint(PointF point, MotionEvent event)
        {
            float x = event.getX(0) + event.getX(1);
            float y = event.getY(0) + event.getY(1);
            point.set(x / 2, y / 2);
        }

    }

}
