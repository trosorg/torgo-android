/*
 * Copyright 2015 Matthew Aguirre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tros.torgo.android.opengl;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import org.tros.logo.android.AndroidCanvas;
import org.tros.torgo.android.ILogoDrawable;
import org.tros.torgo.android.ISizeChangeListener;
import org.tros.utils.AutoResetEvent;

/**
 * Created by matta on 6/21/15.
 */
public class MyGLSurfaceView extends GLSurfaceView implements ILogoDrawable, ISizeChangeListener {

    private static float COLOR_FACTOR = 255.0f;
    private static final float TRANSLATE_FACTOR = 1000.0f;
    private MyGLRenderer mRenderer;
    private float[] color = new float[]{0.0f, 0.0f, 0.0f, 1.0f};
    private final org.apache.commons.lang3.event.EventListenerSupport<Runnable> onSizeChange = org.apache.commons.lang3.event.EventListenerSupport.create(Runnable.class);

    public MyGLSurfaceView(Context context)
    {
        super(context);
        init();
    }

    public MyGLSurfaceView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void requestRender() {
        final AutoResetEvent are = new AutoResetEvent(false);
        super.requestRender();
        queueEvent(new Runnable() {
            @Override
            public void run() {
                //add a new line in the render thread:
                are.set();
            }
        });
        try {
            //wait for the requested render to complete:
            are.waitOne();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void init(){
        AndroidCanvas.IMPLIED_PEN_UP_ON_BACKWARD = true;

        // Create an OpenGL ES 2.0 context
        setEGLContextClientVersion(2);

        mRenderer = new MyGLRenderer();
        // Set the Renderer for drawing on the GLSurfaceView
        setRenderer(mRenderer);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        onSizeChange.fire().run();
    }

    @Override
    public void setPaintColor(String color) {

    }

    @Override
    public void setCanvasColor(String color) {
    }

    @Override
    public void setPaintColor(int r, int g, int b, int a) {
        color[0] = (float) r / COLOR_FACTOR;
        color[1] = (float) g / COLOR_FACTOR;
        color[2] = (float) b / COLOR_FACTOR;
        color[3] = (float) a / COLOR_FACTOR;
    }

    @Override
    public void setCanvasColor(int r, int g, int b) {
        //set the background color (max out alpha):
        mRenderer.setBgColor(r, g, b, (int) COLOR_FACTOR);
    }

    @Override
    public void drawString(String text, float x, float y, float angle) {
    }

    @Override
    public void drawLine(final float x1, final float y1, final float x2, final float y2) {
        final Lines.Line l = new Lines.Line(x1, y1, x2, y2);
        l.setColor(color[0], color[1], color[2], color[3]);

        queueEvent(new Runnable() {
            @Override
            public void run() {
                //add a new line in the render thread:
                mRenderer.addLine(l);
            }
        });
    }

    @Override
    public void clearCanvas() {
        queueEvent(new Runnable() {
            @Override
            public void run() {
                //clear all lines:
                mRenderer.clear();
            }
        });
    }

    @Override
    public void showTurtle(boolean value) {
        mRenderer.showTurtle(value);
    }

    public void turtleUpdate(float angle, float x, float y) {
        //this is called during repaint() which is called at the end of every command in LOGO
        mRenderer.turtlePos(angle, x, y);
        requestRender();
    }

    @Override
    public void addOnSizeChangeListener(Runnable runner) {
        onSizeChange.addListener(runner);
    }

    private float sizeCoef = 1;
    private class ScaleDetectorListener implements ScaleGestureDetector.OnScaleGestureListener{

        float scaleFocusX = 0;
        float scaleFocusY = 0;

        public boolean onScale(ScaleGestureDetector arg0) {
            float scale = arg0.getScaleFactor() * sizeCoef;

            sizeCoef = scale;
            mRenderer.scale = sizeCoef;

            requestRender();

            return true;
        }

        public boolean onScaleBegin(ScaleGestureDetector arg0) {
            invalidate();

            scaleFocusX = arg0.getFocusX();
            scaleFocusY = arg0.getFocusY();

            return true;
        }

        public void onScaleEnd(ScaleGestureDetector arg0) {
            scaleFocusX = 0;
            scaleFocusY = 0;
        }
    }

    private class GestureDetectorListener implements GestureDetector.OnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public void onShowPress(MotionEvent e) {

        }

        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            mRenderer.xTranslate += distanceX / TRANSLATE_FACTOR;
            mRenderer.yTranslate += distanceY / TRANSLATE_FACTOR;
            requestRender();
            return true;
        }

        @Override
        public void onLongPress(MotionEvent e) {

        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            return false;
        }
    }

    ScaleGestureDetector mScaleDetector = new ScaleGestureDetector(getContext(),
            new ScaleDetectorListener());
    GestureDetector mGestureDetector = new GestureDetector(getContext(),
            new GestureDetectorListener());

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mScaleDetector.onTouchEvent(event) && mGestureDetector.onTouchEvent(event)) {
            return true;
        }

        return super.onTouchEvent(event);
    }
}