/*
 * Copyright 2015 Matthew Aguirre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tros.torgo.android;

/**
 * Created by matta on 6/13/15.
 */
public interface ILogoDrawable {
    void setPaintColor(String color);

    void setCanvasColor(String color);

    void setPaintColor(int r, int g, int b, int a);

    void setCanvasColor(int r, int g, int b);

    void drawString(String text, float x, float y, float angle);

    void drawLine(float x1, float y1, float x2, float y2);

    void clearCanvas();

    int getWidth();

    int getHeight();

    void postInvalidate();

    void showTurtle(boolean value);

    void turtleUpdate(float angle, float x, float y);

    void setKeepScreenOn(boolean value);
}
