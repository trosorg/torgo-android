/*
 * Copyright 2015 Matthew Aguirre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tros.torgo.android;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import org.apache.commons.io.IOUtils;
import org.tros.torgo.R;

import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;


public class SourceEditActivity extends ActionBarActivity {

    private static final org.tros.utils.logging.Logger logger = org.tros.utils.logging.Logging.getLogFactory().getLogger(SourceEditActivity.class);
    public final static String SOURCE_MESSAGE = "org.tros.torgo.SOURCE";

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        String font_size = settings.getString("font_size", "");
        if (!"".equals(font_size)) {
            try {
                int f = Integer.parseInt(font_size);
                EditText t = (EditText) findViewById(R.id.input_text);
                t.setTextSize(TypedValue.COMPLEX_UNIT_DIP, f);
            } catch(NumberFormatException nfe) {}
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_source_edit);

        final Spinner scripts = (Spinner) findViewById(R.id.script_select);
        List<String> list = new ArrayList<String>();

        try {
            InputStream manifest = TorgoApp.getContext().getAssets().open("logo/examples/antlr/resource.manifest");
            List<String> lines = IOUtils.readLines(manifest);
            for (String line : lines) {
                list.add("logo/examples/antlr/" + line);
            }

            manifest = TorgoApp.getContext().getAssets().open("logo/examples/tortue/resource.manifest");
            lines = IOUtils.readLines(manifest);
            for (String line : lines) {
                list.add("logo/examples/tortue/" + line);
            }
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            scripts.setAdapter(dataAdapter);
        } catch (Exception ex) {
            logger.fatal(ex.getMessage());
        }

        final EditText editor = (EditText) findViewById(R.id.input_text);
        scripts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    String script_resource = String.valueOf(scripts.getSelectedItem());
                    InputStream systemResourceAsStream = TorgoApp.getContext().getAssets().open(script_resource);

                    StringWriter writer = new StringWriter();
                    IOUtils.copy(systemResourceAsStream, writer);
                    String source = writer.toString();
                    editor.setText(source);
                } catch (Exception ex) {
                    logger.fatal(ex.getMessage());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                editor.setText("");
            }
        });
        final Button button = (Button) findViewById(R.id.parse_button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(SourceEditActivity.this, LogoActivity.class);
                intent.putExtra(SOURCE_MESSAGE, editor.getText().toString());
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_source_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(this, SettingsActivity.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
