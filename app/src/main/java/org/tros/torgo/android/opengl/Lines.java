/*
 * Copyright 2015 Matthew Aguirre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tros.torgo.android.opengl;

import android.opengl.GLES20;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

/**
 * Created by matta on 6/23/15.
 */
public class Lines implements Renderable {
    private static final String vertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of the objects that use this vertex shader
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +
                    // the matrix must be included as a modifier of gl_Position
                    // Note that the uMVPMatrix factor *must be first* in order
                    // for the matrix multiplication product to be correct.
                    "  gl_Position = uMVPMatrix * vPosition;" +
                    "}";

    private static final String fragmentShaderCode =
            "precision mediump float;" +
                    "uniform vec4 vColor;" +
                    "void main() {" +
                    "  gl_FragColor = vColor;" +
                    "}";

    private int mProgram;
    private static final int vertexCount = 2;
    private static final int vertexStride = Line.COORDS_PER_VERTEX * 4; // 4 bytes per vertex

    private final ArrayList<Line> lines;

    public Lines() {
        lines = new ArrayList<Line>();

        // prepare shaders and OpenGL program
        int vertexShader = MyGLRenderer.loadShader(
                GLES20.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = MyGLRenderer.loadShader(
                GLES20.GL_FRAGMENT_SHADER, fragmentShaderCode);

        mProgram = GLES20.glCreateProgram();             // create empty OpenGL Program
        GLES20.glAttachShader(mProgram, vertexShader);   // add the vertex shader to program
        GLES20.glAttachShader(mProgram, fragmentShader); // add the fragment shader to program
        GLES20.glLinkProgram(mProgram);                  // create OpenGL program executables
    }

    protected void addLine(Line l) {
        lines.add(l);
    }

    protected void clear() {
        lines.clear();
    }

    @Override
    public void draw(float[] mvpMatrix) {
        int mMVPMatrixHandle;
        int mPositionHandle;
        int mColorHandle;

        GLES20.glLineWidth(1.0f);
        // Add program to OpenGL ES environment
        GLES20.glUseProgram(mProgram);

        // get handle to vertex shader's vPosition member
        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

        // Enable a handle to the triangle vertices
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // get handle to fragment shader's vColor member
        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");

        // get handle to shape's transformation matrix
        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");

        // Pass the projection and view transformation to the shader
        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);

        for(Line l : lines) {
            // Prepare the triangle coordinate data
            GLES20.glVertexAttribPointer(mPositionHandle, Line.COORDS_PER_VERTEX,
                    GLES20.GL_FLOAT, false,
                    vertexStride, l.vertexBuffer);

            // Set color for drawing the triangle
            GLES20.glUniform4fv(mColorHandle, 1, l.color, 0);

            // Draw the line
            GLES20.glDrawArrays(GLES20.GL_LINES, 0, vertexCount);
        }

        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }

    public static class Line {
        public static final float factor = 250.f;
        private FloatBuffer vertexBuffer;

        // number of coordinates per vertex in this array
        public static final int COORDS_PER_VERTEX = 3;
        private float lineCoords[] = {   // in counterclockwise order:
                0.0f,  0.0f, 0.0f, // pt1
                -0.5f, -0.5f, 0.0f // pt2
        };

        // Set color with red, green, blue and alpha (opacity) values
        private float color[] = { 0.0f, 0.0f, 0.0f, 1.0f };

        public void setColor(int r, int g, int b, int a) {
            color[0] = ((float) r) / 255.0f;
            color[1] = ((float) g) / 255.0f;
            color[2] = ((float) b) / 255.0f;
            color[3] = ((float) a) / 255.0f;
        }

        public void setColor(float r, float g, float b, float a) {
            color[0] = r;
            color[1] = g;
            color[2] = b;
            color[3] = a;
        }

        public Line(float x1, float y1, float x2, float y2) {
            lineCoords[0] = x1 / factor;
            lineCoords[1] = y1 / factor;
            lineCoords[3] = x2 / factor;
            lineCoords[4] = y2 / factor;

            // initialize vertex byte buffer for shape coordinates
            ByteBuffer bb = ByteBuffer.allocateDirect(
                    // (number of coordinate values * 4 bytes per float)
                    lineCoords.length * 4);
            // use the device hardware's native byte order
            bb.order(ByteOrder.nativeOrder());

            // create a floating point buffer from the ByteBuffer
            vertexBuffer = bb.asFloatBuffer();
            // add the coordinates to the FloatBuffer
            vertexBuffer.put(lineCoords);
            // set the buffer to read the first coordinate
            vertexBuffer.position(0);
        }
    }
}
