/*
 * Copyright 2015 Matthew Aguirre
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.tros.torgo;

import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.lang3.event.EventListenerSupport;
import org.tros.torgo.interpreter.CodeBlock;
import org.tros.torgo.interpreter.InterpreterListener;
import org.tros.torgo.interpreter.InterpreterThread;
import org.tros.torgo.interpreter.Scope;
import org.tros.utils.AutoResetEvent;

/**
 * The main application. Controls GUI and interpreting process.
 *
 * @author matta
 */
public abstract class ControllerBase implements Controller {

    private InterpreterThread interp;
    protected final AutoResetEvent step;
    protected final AtomicBoolean isStepping;
    private String source;

//    private final ArrayList<JCheckBoxMenuItem> viz = new ArrayList<JCheckBoxMenuItem>();

    protected final EventListenerSupport<InterpreterListener> listeners
            = EventListenerSupport.create(InterpreterListener.class);

    /**
     * Add a listener
     *
     * @param listener
     */
    @Override
    public void addInterpreterListener(InterpreterListener listener) {
        listeners.addListener(listener);
    }

    /**
     * Remove a listener
     *
     * @param listener
     */
    @Override
    public void removeInterpreterListener(InterpreterListener listener) {
        listeners.removeListener(listener);
    }

    /**
     * Hidden Constructor.
     */
    protected ControllerBase() {
        source = "";
        step = new AutoResetEvent(false);
        isStepping = new AtomicBoolean(false);
    }

    /**
     * Create an interpreter thread for the desired language.
     *
     * @param source
     * @return
     */
    protected abstract InterpreterThread createInterpreterThread(String source);

    /**
     * Sets up the environment.
     */
    @Override
    public final void run() {
        newFile();
        runHelper();
    }

    /**
     * Helper method called during run().
     */
    protected void runHelper() {
    }

    /**
     * Initialize the GUI back to initial state.
     */
    private void init() {
        stopInterpreter();
    }

    /**
     * Open a file based on URL.
     *
     * @param file
     */
    @Override
    public void openFile(URL file) {
//        try {
//            init();
//            StringWriter writer = new StringWriter();
//            IOUtils.copy(file.openStream(), writer);
//            this.setSource(writer.toString());
//            //handle windows, jar, and linux path.  Not sure if necessary, but should work.
//            String toSplit = file.getFile().replace("/", "|").replace("\\", "|");//.split("|");
//            String[] split = toSplit.split("\\|");
//            this.window.setTitle("Torgo - " + split[split.length - 1]);
//        } catch (IOException ex) {
//            init();
//            LogFactory.getLog(ControllerBase.class).fatal(null, ex);
//        }
    }

    /**
     * Open a file.
     */
    @Override
    public void openFile() {
//        JFileChooser chooser = new JFileChooser();
//        chooser.setMultiSelectionEnabled(false);
//        java.util.prefs.Preferences prefs = java.util.prefs.Preferences.userNodeForPackage(ControllerBase.class);
//        chooser.setCurrentDirectory(new File(prefs.get(ControllerBase.class.getName() + "-working-directory", ".")));
//
//        if (chooser.showOpenDialog(window) == JFileChooser.APPROVE_OPTION) {
//            filename = chooser.getSelectedFile().getPath();
//            prefs.put(ControllerBase.class.getName() + "-working-directory", chooser.getSelectedFile().getParent());
//            openFile(chooser.getSelectedFile());
//        }
    }

    /**
     * Save the script as a new file.
     */
    @Override
    public void saveFileAs() {
//        JFileChooser chooser = new JFileChooser();
//        chooser.setMultiSelectionEnabled(false);
//        java.util.prefs.Preferences prefs = java.util.prefs.Preferences.userNodeForPackage(ControllerBase.class);
//        chooser.setCurrentDirectory(new File(prefs.get(ControllerBase.class.getName() + "-working-directory", ".")));
//
//        int result = chooser.showSaveDialog(window);
//
//        if (result == JFileChooser.APPROVE_OPTION) {
//            filename = chooser.getSelectedFile().getPath();
//            prefs.put(ControllerBase.class.getName() + "-working-directory", chooser.getSelectedFile().getParent());
//            saveFile();
//        }
    }

    /**
     * Save the script.
     */
    @Override
    public void saveFile() {
//        if (filename == null) {
//            saveFileAs();
//            return;
//        }
//
//        try {
//            FileOutputStream out = new FileOutputStream(filename);
//            String source = torgoPanel.getSource();
//            byte[] sourceArray = new byte[source.length()];
//
//            for (int i = 0; i < source.length(); i++) {
//                sourceArray[i] = (byte) source.charAt(i);
//            }
//
//            out.write(sourceArray);
//            window.setTitle("Torgo - " + filename);
//            out.flush();
//            out.close();
//        } catch (Exception ex) {
//            LogFactory.getLog(ControllerBase.class).fatal(null, ex);
//        }
    }

    /**
     * Print (unused)
     */
    @Override
    public void printCanvas() {
        org.tros.utils.logging.Logging.getLogFactory().getLogger(ControllerBase.class).debug("printCanvas() called");
        /*
         PrinterJob printJob = PrinterJob.getPrinterJob();
         PageFormat pageFormat = printJob.defaultPage();
         printJob.setPrintable(torgoCanvas, pageFormat);
         */
    }

    /**
     * Instantiate/Run the interpreter.
     */
    @Override
    public void startInterpreter() {
        if (interp != null) {
            return;
        }
        interp = createInterpreterThread(source);

        //TODO: make this like before...
//        InterpreterVisualization viz = TorgoToolkit.getVisualization("TraceLogger");
//        if (viz != null) {
//            viz.create().watch(this.getLang(), this, interp);
//        }

//        for (JCheckBoxMenuItem item : viz) {
//            if (item.getState()) {
//                TorgoToolkit.getVisualization(item.getText()).create().watch(this.getLang(), this, interp);
//            }
//        }

        for (InterpreterListener l : listeners.getListeners()) {
            interp.addInterpreterListener(l);
        }

        interp.addInterpreterListener(new InterpreterListener() {

            @Override
            public void started() {
            }

            @Override
            public void finished() {
            }

            @Override
            public void error(Exception e) {
                org.tros.utils.logging.Logging.getLogFactory().getLogger(ControllerBase.class).fatal(null, e);
            }

            @Override
            public void message(String msg) {
            }

            @Override
            public void currStatement(CodeBlock block, Scope scope) {
            }
        });

        interp.start();
    }

    /**
     * Instantiate an interpreter in debug mode.
     */
    @Override
    public void debugInterpreter() {
        interp = createInterpreterThread(source);
        step.reset();

//        for (JCheckBoxMenuItem item : viz) {
//            if (item.getState()) {
//                TorgoToolkit.getVisualization(item.getText()).create().watch(this.getLang(), this, interp);
//            }
//        }
//        viz.stream().filter((item) -> (item.getState())).map((item) -> TorgoToolkit.getVisualization(item.getText()).create()).forEach((visualization) -> {
//            visualization.watch(this.getLang(), this, interp);
//        });

        for (InterpreterListener l : listeners.getListeners()) {
            interp.addInterpreterListener(l);
        }

        interp.addInterpreterListener(new InterpreterListener() {

            @Override
            public void started() {
            }

            @Override
            public void finished() {
            }

            @Override
            public void error(Exception e) {
                org.tros.utils.logging.Logging.getLogFactory().getLogger(ControllerBase.class).fatal(null, e);
            }

            @Override
            public void message(String msg) {
            }

            @Override
            public void currStatement(CodeBlock block, Scope scope) {
                try {
                    if (isStepping.get()) {
                        step.waitOne();
                    }
                    //TODO: this needs to be configurable
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    org.tros.utils.logging.Logging.getLogFactory().getLogger(ControllerBase.class).fatal(null, ex);
                }
                int line = block.getParserRuleContext().getStart().getLine();
                int start = block.getParserRuleContext().getStart().getStartIndex();
                int end = block.getParserRuleContext().getStart().getStopIndex();
            }
        });
        interp.start();
    }

    /**
     * Allows stepping through interpreter commands one-at-a-time.
     */
    @Override
    public void stepOver() {
        step.set();
    }

    /**
     * Pause running the interpreter. Used in debug mode.
     */
    @Override
    public void pauseInterpreter() {
        isStepping.set(true);
    }

    /**
     * Resume the interpreter. Used in debug mode.
     */
    @Override
    public void resumeInterpreter() {
        isStepping.set(false);
        step.set();
    }

    /**
     * Stop the interpreter.
     */
    @Override
    public void stopInterpreter() {
        if (interp != null) {
            interp.halt();
            step.set();
            try {
                interp.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            interp = null;
        }
    }

    /**
     * Close the application.
     */
    @Override
    public void close() {
        stopInterpreter();
    }

    /**
     * Insert a command into the input pane.
     *
     * @param command
     */
    @Override
    public void insertCommand(String command) { }

    /**
     * Set the source of the input pane.
     *
     * @param src
     */
    @Override
    public void setSource(String src) {
        source = src;
    }

    @Override
    public void newFile() {
//        if (this.window.isVisible()) {
//            filename = null;
//            window.setTitle("Torgo - " + Localization.getLocalizedString("UntitledLabel"));
//
//            init();
//        }
    }
//    /**
//     * Return the current GUI window. This is made available for setting parents
//     * for dialog windows.
//     *
//     * @return
//     */
//    protected final JFrame getWindow() {
//        return this.window;
//    }
}
