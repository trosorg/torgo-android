package org.tros.utils.logging;

import java.util.HashMap;

/**
 * Created by matta on 6/18/15.
 */
public class AndroidLogFactory implements LogFactory {

    private static final HashMap<String, Logger> instances = new HashMap<String, Logger>();

    @Override
    public Logger getLogger(Class<?> c) {
        return getLogger(c.getName());
    }

    @Override
    public Logger getLogger(String name) {
        synchronized (instances) {
            if (!instances.containsKey(name)) {
                instances.put(name, new AndroidLogger(name));
            }
        }
        return instances.get(name);
    }
}
