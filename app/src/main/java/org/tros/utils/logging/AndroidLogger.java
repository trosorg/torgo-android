package org.tros.utils.logging;

import android.util.Log;

import java.text.MessageFormat;

/**
 * Created by matta on 6/18/15.
 */
public class AndroidLogger implements Logger {

    private final String tag;

    protected AndroidLogger(String tag) {
        this.tag = tag;
    }

    @Override
    public void warn(String message) {
        Log.w(tag, message);
    }

    @Override
    public void debug(String message) {
        Log.d(tag, message);
    }

    @Override
    public void error(String message) {
        Log.e(tag, message);
    }

    @Override
    public void info(String message) {
        Log.i(tag, message);
    }

    @Override
    public void verbose(String message) {
        Log.v(tag, message);
    }

    @Override
    public void fatal(String message) {
        Log.e(tag, message);
    }

    @Override
    public void warn(String format, Object... objs) {
        Log.w(tag, MessageFormat.format(format, objs));

    }

    @Override
    public void debug(String format, Object... objs) {
        Log.d(tag, MessageFormat.format(format, objs));
    }

    @Override
    public void error(String format, Object... objs) {
        Log.e(tag, MessageFormat.format(format, objs));
    }

    @Override
    public void info(String format, Object... objs) {
        Log.i(tag, MessageFormat.format(format, objs));
    }

    @Override
    public void verbose(String format, Object... objs) {
        Log.v(tag, MessageFormat.format(format, objs));
    }

    @Override
    public void fatal(String format, Object... objs) {
        Log.e(tag, MessageFormat.format(format, objs));
   }

    @Override
    public void warn(String message, Throwable thrw) {
        Log.w(tag, message, thrw);
    }

    @Override
    public void debug(String message, Throwable thrw) {
        Log.d(tag, message, thrw);
    }

    @Override
    public void error(String message, Throwable thrw) {
        Log.e(tag, message, thrw);
    }

    @Override
    public void info(String message, Throwable thrw) {
        Log.i(tag, message, thrw);
    }

    @Override
    public void verbose(String message, Throwable thrw) {
        Log.v(tag, message, thrw);
    }

    @Override
    public void fatal(String message, Throwable thrw) {
        Log.e(tag, message, thrw);
    }
}
