#Torgo for Android
[![Build Status](https://travis-ci.org/ZenHarbinger/torgo-android.svg?branch=master)](https://travis-ci.org/ZenHarbinger/torgo-android)

##Done:
 1. [Torgo](https://github.com/ZenHarbinger/torgo) code switched to Java6.
 2. [ANTLR ported to Android](https://github.com/ZenHarbinger/antlr4).

##Goals:
 1. Target Android 5.0.1
 2. Allow the same flexibility of interpreter implementation as the desktop version.

##TODO:
 1. Figure out Android UI design.
 2. Script input.
 3. Script output.
